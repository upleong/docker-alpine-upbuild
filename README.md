# docker-alpine-upbuild

## Purpose
Running on host machine with docker to build package for alpine linux
(e.g. Raspberry Pi)


## Usage - Build with modifications
### mpv with rpi-mmal enabled
The file `up-aports/macodigo/mpv/APKBUILD` is modified by adding
dependency `raspberrypi-dev` and configuration flags `--enable-rpi --enable-rpi-mmal`

```
    make build-armv7
    make run-armv7

    (cd /up-aports/macodigo/mpv/; abuild -r)
```

### Reference
https://gitlab.alpinelinux.org/alpine/infra/docker/aports-build.git

### Docker Hub
Avaialble at, https://hub.docker.com/r/macodigo/alpine-upbuild/
