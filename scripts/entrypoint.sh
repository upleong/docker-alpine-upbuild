#!/bin/sh

set -euo pipefail

: "${MQTT_HOSTNAME:=mqtt}"
: "${APORTS_REPO:=https://gitlab.alpinelinux.org/alpine/aports.git}"
: "${BRANCH:=master}"
: "${HOSTNAME:=$(hostname)}"
: "${EXTRA_PACKAGES:=}"
: "${REPOSITORIES:=}"
: "${REPO_FILE:=$(mktemp)}"
: "${FORCE_BOOTSTRAP:=}"

if [ -z "$REPOSITORIES" ] ; then
	case "$BRANCH" in
		master) REPOSITORIES="main community testing";;
		*.*-stable) REPOSITORIES="main community";;
		*) echo "\"$BRANCH\" unknown branch"; exit 1;;
	esac
fi	

# create or update directory permissions
# TODO: mqtt-exec run dir should be fixed upstream
doas install -d -o buildozer -g buildozer \
	/home/buildozer/.ssh \
	/home/buildozer/.abuild \
	/home/buildozer/packages \
	/home/buildozer/aports \
	/var/run/mqtt-exec.aports-build

doas install -d -m 775 -o root -g abuild \
	/var/cache/distfiles \
	/var/cache/distfiles/buildlogs

# generate a ssh key to upload logs and pkgs
if ! [ -f "$HOME/.ssh/id_ed25519" ]; then
	ssh-keygen -q -t ed25519 -f $HOME/.ssh/id_ed25519 -N ""
fi

if ! abuild-sign --installed; then
	# if [ -z "$PACKAGER" ]; then
	# 	echo "abuild private key not found and \$PACKAGER not set. Aborting."
	# 	exit 1
	# fi
	SUDO=doas abuild-keygen -ain
	cat <<- EOF >> "$HOME"/.abuild/abuild.conf
	export JOBS=\$(nproc)
	export MAKEFLAGS=-j\$JOBS
	export SAMUFLAGS=-j\$JOBS
	EOF
fi

doas cp /home/buildozer/.abuild/*.rsa.pub /etc/apk/keys

# check if we have our own build-base packages
printf "$HOME/packages/%s\\n" $REPOSITORIES >> "$REPO_FILE"
printf "$HOME/packages/macodigo\\n" >> "$REPO_FILE"

# if [ -d ""$HOME"/aports/main" ]; then
# 	git -C $HOME/aports pull
# else
# 	git -C $HOME clone "$APORTS_REPO"
# fi

# HAS_BUILD_BASE="$(apk --repositories-file="$REPO_FILE" search -x build-base 2> /dev/null)"

# if [ ! "$HAS_BUILD_BASE" ] || [ "$FORCE_BOOTSTRAP" ]; then
# 	doas apk -U upgrade -a
# 	cd "$HOME"/aports/main
# 	cat /etc/apk/world | xargs ap recursdeps 2>/dev/null | sort | uniq |
# 		xargs ap builddirs 2>/dev/null |
# 		xargs -n1 -I%% sh -c 'cd "%%" && abuild -r'
# fi

# update system with local build packages
doas install -m644 "$REPO_FILE" /etc/apk/repositories
# doas apk -U upgrade -a

# install some packages that are not in depends due to bootstrap issues
[ "$EXTRA_PACKAGES" ] && doas apk add "$EXTRA_PACKAGES"

# exec mqtt-exec -v -h "$MQTT_HOSTNAME" \
# 	-t git/aports/"$BRANCH" \
# 	-t git/aports/"$BRANCH"/release \
# 	--will-topic build/"$HOSTNAME" \
# 	--will-retain \
# 	-- /usr/bin/aports-build

echo "Ready to build"
exec /bin/ash
