FROM --platform=${TARGETPLATFORM:-linux/amd64} alpine:edge
MAINTAINER UP Leong <pio.leong@gmail.com>

RUN apk update
RUN apk upgrade --no-cache -a && \
	apk add --no-cache aports-build build-base git doas && \
	adduser buildozer abuild && adduser buildozer wheel

RUN echo "permit nopass buildozer as root" >> /etc/doas.conf

COPY scripts /usr/local/bin

WORKDIR /home/buildozer

USER buildozer

ENTRYPOINT  [ "entrypoint.sh" ]
