TAGNAME=macodigo/alpine-upbuild
CONTAINER_NAME=alpine-upbuild

.PHONY: all build

prepare:
	mkdir -p build

all:
	@echo "macodigo/alpine-upbuild with docker"

#
define build-image
	docker build --platform linux/${1} -t ${TAGNAME}:$(shell echo ${1} | sed 's/\///g') -f Dockerfile .
endef

define run-in-container
	docker run --platform linux/${1} -it --rm -v ${PWD}/build:/home/buildozer -v ${PWD}/up-aports:/up-aports --name ${CONTAINER_NAME}-$(shell echo ${1} | sed 's/\///g') ${TAGNAME}
endef

define run-in-container-noname
	docker run --platform linux/${1} -it --rm -v ${PWD}/build:/home/buildozer -v ${PWD}/up-aports:/up-aports ${TAGNAME}
endef

define exec-in-container
	docker exec -it ${CONTAINER_NAME}-$(shell echo ${1} | sed 's/\///g') ash
endef


#
build: build-amd64 build-arm64 build-armv7

build-amd64:
	$(call build-image,amd64)

build-arm64:
	$(call build-image,arm64)

build-armv7:
	$(call build-image,arm/v7)

# NOTE: images need to be pushed before creating manifest
create-manifest:
	docker manifest create ${TAGNAME} ${TAGNAME}:amd64 ${TAGNAME}:arm64 ${TAGNAME}:armv7

push-manifest:
	docker manifest push ${TAGNAME}

inspect-manifest:
	docker manifest inspect ${TAGNAME}

#
push-amd64:
	docker push ${TAGNAME}:amd64

push-arm64:
	docker push ${TAGNAME}:arm64

push-armv7:
	docker push ${TAGNAME}:armv7

push-all:
	for arch in amd64 arm64 armv7; do docker push ${TAGNAME}:$${arch}; done


# for directly running with docker
run-amd64:
	$(call run-in-container,amd64)

run-arm64:
	$(call run-in-container,arm64)

run-armv7:
	$(call run-in-container,arm/v7)

#
exec-amd64:
	$(call exec-in-container,amd64)

exec-arm64:
	$(call exec-in-container,arm64)

exec-armv7:
	$(call exec-in-container,arm/v7)

#
debug-run-amd64:
	$(call run-in-container,amd64)

debug-run-arm64:
	$(call run-in-container,arm64)

debug-run-armv7:
	$(call run-in-container,arm/v7)

#
stop:
	for arch in amd64 arm64 arm/v7; do docker stop ${CONTAINER_NAME}-$${arch}; done

rm:
	for arch in amd64 arm64 arm/v7; do docker rm ${CONTAINER_NAME}-$${arch}; done
